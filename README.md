# DevSecOps com OpenSource - Aplicações mais seguras!

Repositório para armazenar o Laboratório e slides da palestra realizada por mim no Join Community 2022 em Goiânia - Goiás

## whoami

### 🇧🇷 **Samuel Gonçalves Pereira**

* Tech Lead na 4Linux
* Consultor de Tecnologia nas áreas **Segurança Ofensiva** e **DevSecOps**
* Mais de 10 anos de experiência 💻
* *"3k++"* alunos ensinados no 🌎
* Músico, Contista, YouTuber e Podcaster
* Apaixonado por Software Livre 💙

> Contato: [https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

## Objetivos da Palestra

1. Compreender os princípios do DevSecOps
2. Implementar uma pipeline funcional com SAST e DAST usando:
   1. Jenkins
   2. Horusec
   3. Golismero
   4. Git
3. Listar ferramentas OpenSource para Segurança Ágil
4. Solucionar dúvidas relacionadas a Segurança no processo de desenvolvimento

## Laboratório

### Dependências

Para a criação do laboratório é necessário ter pré instalado os seguintes softwares:

* [Git][2]
* [VirtualBox][3]
* [Vagrant][5]

> Para o máquinas com Windows aconselhamos o uso do proprio Powershell e que as instalações dos softwares são feitas da maneira tradicional

> Para as máquinas com MAC OS aconselhamos, se possível, que as instalações sejam feitas pelo gerenciador de pacotes **brew**.

### Descrição do Ambiente

O Laboratório será criado utilizando o [Vagrant][7]. Ferramenta para criar e gerenciar ambientes virtualizados (baseado em Inúmeros providers) com foco em automação.

Nesse laboratórios, que está centralizado no arquivo [Vagrantfile][8], serão criadas 2 maquinas com as seguintes características:

Nome       | vCPUs | Memoria RAM | IP            | S.O.           | Script de Provisionamento
---------- |:-----:|:-----------:|:-------------:|:---------------:| -----------------------------
testing    | 1     | 2048MB      | 192.168.56.50 | almalinux/8     | [provisionamento/testing.yaml](provisionamento/testing.yaml)
automation | 1     | 2048MB      | 192.168.56.60 | almalinux/8     | [provisionamento/automation.yaml](provisionamento/automation.yaml)

### Criação do Laboratório

Para criar o laboratório é necessário fazer o `git clone` desse repositório e, dentro da pasta baixada realizar a execução do `vagrant up`, conforme abaixo:

```bash
git clone https://gitlab.com/o_sgoncalves/devsecops-joincommunity-2022
cd devsecops-joincommunity-2022/
vagrant up
```

_O Laboratório **pode demorar**, dependendo da conexão de internet e poder computacional, para ficar totalmente preparado._

### Dicas de utilização

Por fim, para melhor utilização, abaixo há alguns comandos básicos do vagrant para gerencia das máquinas virtuais.

Comandos                | Descrição
:----------------------:| ---------------------------------------
`vagrant init`          | Gera o VagrantFile
`vagrant box add <box>` | Baixar imagem do sistema
`vagrant box status`    | Verificar o status dos boxes criados
`vagrant up`            | Cria/Liga as VMs baseado no VagrantFile
`vagrant provision`     | Provisiona mudanças logicas nas VMs
`vagrant status`        | Verifica se VM estão ativas ou não.
`vagrant ssh <vm>`      | Acessa a VM
`vagrant ssh <vm> -c <comando>` | Executa comando via ssh
`vagrant reload <vm>`   | Reinicia a VM
`vagrant halt`          | Desliga as VMs

> Para maiores informações acesse a [Documentação do Vagrant][13]

## Repositório da Aplicação

O repositório da aplicação com todos os arquivos configurados está disponível no link: [gitlab.com/o_sgoncalves/juice-shop-teste](https://gitlab.com/o_sgoncalves/juice-shop-teste)
## Horusec

Para começar a configurar o Horusec vamos acessar a máquina `automation` com o comando:
```shell=
vagrant ssh automation
```

Eleve seus privilégios aos do usuário `root` com o comando:
```shell=
sudo -i
```

Acesse o diretório `/opt/horusec` e realize a instalação com os comandos:
```shell=
cd /opt/horusec
make install
```

O provisionamento irá levar alguns minutos, é normal.

As credenciais padrão são:
```text=
email: dev@example.com
password: Devpass0*
```

Criar chave de API para projeto.

## Juice Shop

Repositório da loja de sucos: [https://github.com/juice-shop/juice-shop](https://github.com/juice-shop/juice-shop)

## Jenkins

Para começar a configurar o Jenkins vamos acessar a máquina `testing` com o comando:
```shell=
vagrant ssh testing
```

Eleve seus privilégios aos do usuário `root` com o comando:
```shell=
sudo -i
```

Agora precisaremos verificar a senha inicial criada durante a instalação do jenkins, para isso execute o comando:
```shell=
cat /var/lib/jenkins/secrets/initialAdminPassword
```

Copie o conteúdo, e cole na tela do Jenkins.

Feito isso vamos instalar os seguintes plugins:
* Docker
* Docker Pipeline
* ChuckNorris
* HTML Publisher
* Blue Ocean

Configurar agente do Jenkins "testing" e "automation"

Depois configure uma credencial com a Chave de API do Horusec. O nome precisa ser `api-key-horusec`.

Jenkinsfile completo:
```groovy=
pipeline{
    agent any
    environment {
        NAME_APP = "juice-shop"
    }

    stages {
        stage('Repositório de Código'){
          steps{
            git url: 'https://gitlab.com/o_sgoncalves/juice-shop-teste',
                branch: 'master'
            stash 'devsec-repositorio'
          }
        }

        stage('SAST - Horusec') {
            environment {
                // variaveis especifcas para o OWASP ZAP
                HORUSEC_API_KEY = credentials('api-key-horusec')
            }
            // Configuração do Agent automation
            agent { node 'automation' }
            steps {
                sh 'horusec start -a $HORUSEC_API_KEY -u http://192.168.56.60:8000 -p ${WORKSPACE}/ -G true --enable-git-history="true"'
            }
        }

        stage('Build + Executando Container'){
            agent { node 'automation' }
            steps {
                sh "docker-compose up -d"
            }
        }

        stage('DAST com Golismero'){
            environment {
                APP_URL = "http://192.168.56.60"
            }
            agent { node 'testing' }
            steps{
                sh "/opt/golismero/golismero.py scan $APP_URL -o dast-report-${BUILD_NUMBER}.html"
                publishHTML([
                    allowMissing: false,
                    alwaysLinkToLastBuild: true,
                    keepAll: true,
                    reportDir: '.',
                    reportFiles: 'dast-report-${BUILD_NUMBER}.html',
                    reportName: 'DAST REPORT'
                ])
            }
        }

    }

    post {
        always{
            chuckNorris()
        }
        success {
            echo "Pipeline executado com sucesso!!"
        }
        failure {
            echo "Pipeline falhou!"
        }
    }
}
```

Arquivo docker-compose.yaml:

```yaml=
version: "3.8"
services:
    juice-shop:
        image: juice-shop:latest
        build:
            context: ./
            dockerfile: Dockerfile
        restart: always
        ports:
            - '80:3000'
```


[1]: https://4linux.com.br
[2]: https://git-scm.com/downloads
[3]: https://www.virtualbox.org/wiki/Downloads
[5]: https://www.vagrantup.com/downloads
[6]: https://cygwin.com/install.html
[7]: https://www.vagrantup.com/
[8]: ./Vagrantfile
[9]: ./provisionamento/testing.sh
[10]: ./provisionamento/automation.sh
[11]: ./provisionamento/logging.sh
[12]: ./provisionamento/validation.sh
[13]: https://www.vagrantup.com/docs
[14]: https://app.vagrantup.com/4linux